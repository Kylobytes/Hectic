// Copyright (C) 2018  Kent Delante

// This file is part of Hectic
// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

package com.kylobytes.hectic.data.plan

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PlanRepository @Inject constructor(private val planDao: PlanDao) {

    fun loadPlansByDate(date: Long) = planDao.loadPlansByDate(date)k

    suspend fun insert(plan: Plan) =  withContext(Dispatchers.IO) {
        planDao.insert(plan)
    }

    suspend fun delete(plan: Plan) =  withContext(Dispatchers.IO) {
        planDao.delete(plan)
    }

    suspend fun update(plan: Plan) = withContext(Dispatchers.IO) {
        planDao.update(plan)
    }
}