// Copyright (C) 2018  Kent Delante

// This file is part of Hectic
// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

package com.kylobytes.hectic.data.plan

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "plans")
data class Plan(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,

        @NonNull
        val details: String,

        val comments: String = "",

        @ColumnInfo(name = "start_time")
        val startTime: Long?,

        @ColumnInfo(name = "end_time")
        val endTime: Long?,

        @ColumnInfo(name = "scheduled_date")
        val scheduledDate: Long,

        @ColumnInfo(name = "date_created")
        val dateCreated: Long
)