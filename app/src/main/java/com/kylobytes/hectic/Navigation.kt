/*
 * Copyright (C) 2021  Kent Delante <leftybournes@pm.me>
 *
 * This file is part of Hectic.
 *
 * Hectic  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hectic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hectic.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kylobytes.hectic

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltNavGraphViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.kylobytes.hectic.ui.plandetail.PlanDetail
import com.kylobytes.hectic.ui.planner.Planner
import com.kylobytes.hectic.ui.planner.PlannerViewModel

@Composable
fun HecticNavGraph() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = "planner"
    ) {
        composable("planner") {
            val plannerViewModel = hiltNavGraphViewModel<PlannerViewModel>()
            Planner(navController, plannerViewModel)
        }
        composable("plan detail") { PlanDetail() }
    }
}