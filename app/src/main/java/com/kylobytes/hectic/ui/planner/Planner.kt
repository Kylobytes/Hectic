/*
 * Copyright (C) 2021  Kent Delante <leftybournes@pm.me>
 *
 * This file is part of Hectic.
 *
 * Hectic  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hectic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hectic.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kylobytes.hectic.ui.planner

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.navigation.NavController

@Composable
fun Planner(navController: NavController, viewModel: PlannerViewModel) {
    val currentDate by viewModel.currentDate.observeAsState("")

    Scaffold(
        topBar = { TopBar(currentDate) },
        floatingActionButton = { FloatingActionButton() }
    ) {
        Column {
            Text(text = "Planner")
        }
    }
}