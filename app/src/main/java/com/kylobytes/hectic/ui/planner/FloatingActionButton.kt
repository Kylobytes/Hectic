/*
 * Copyright (C) 2021  Kent Delante <leftybournes@pm.me>
 *
 * This file is part of Hectic.
 *
 * Hectic  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hectic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hectic.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kylobytes.hectic.ui.planner

import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.kylobytes.hectic.R

@Composable
fun FloatingActionButton() {
    ExtendedFloatingActionButton(
        onClick = {},
        text = { Text(stringResource(R.string.add_plan)) },
        icon = { Icon(Icons.Filled.Add, stringResource(R.string.add_plan)) }
    )
}