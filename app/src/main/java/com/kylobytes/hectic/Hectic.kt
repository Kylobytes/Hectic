/*
 * Copyright (C) 2021  Kent Delante <leftybournes@pm.me>
 *
 * This file is part of Hectic.
 *
 * Hectic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hectic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hectic.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kylobytes.hectic

import android.app.Application
import android.content.Context
import com.kylobytes.hectic.util.Constants
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Hectic : Application() {
    override fun onCreate(){
        val useDarkTheme = getSharedPreferences(
            Constants.PREFS_THEME_NAME,
            Context.MODE_PRIVATE
        ).getBoolean(Constants.PREF_DARK_THEME, false)

        if (useDarkTheme) {
            setTheme(R.style.AppThemeDark)
        }

        super.onCreate()
    }
}